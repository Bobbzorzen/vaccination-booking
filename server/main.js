'use strict';

const request = require('request');
Intl = require("intl")

var express = require('express');
var cors = require('cors');
var app = express();
app.use(cors());
app.set("port", 5501);


const allCareCentersUrl = "https://rjl.se/api/BookableTimesApi?daysinadvance=30";
const vaccinationUrl = "https://booking-api.mittvaccin.se/clinique/<careCenterId>/appointments/8225/slots/<startDate>-<endDate>";



app.get('/', function(req, res) {
    res.writeHead(200, {
        'Content-Type': 'application/json'
    });
    var response = {
        "response": "This is GET method."
    }
    res.end(JSON.stringify(response));
})

app.get('/care-centers', function(req, res) {
    request(allCareCentersUrl, function(error, response, body) {
        console.log("body: ", body);
        res.writeHead(200, {
            'Content-Type': 'application/json'
        });
        res.end(body);
    })
})

app.get('/care-centers/:id', function(req, res) {

    const currentDate = new Date();
    const endDateObject = new Date();
    endDateObject.setDate(endDateObject.getDate() + 7);

    const startDate = (new Intl.DateTimeFormat('sv', {
        year: '2-digit',
        month: "2-digit",
        day: "2-digit"
    }).format(currentDate)).replace(/[-\/]/gi, "").substring(2);
    console.log("startDate: ", startDate);
    const endDate = (new Intl.DateTimeFormat('sv', {
        year: '2-digit',
        month: "2-digit",
        day: "2-digit"
    }).format(endDateObject)).replace(/[-\/]/gi, "").substring(2);

    const url = vaccinationUrl.replace("<careCenterId>", req.params.id).replace("<startDate>", startDate).replace("<endDate>", endDate);
    console.log("URL: ", url)
    request(url, function(error, response, body) {
        res.writeHead(200, {
            'Content-Type': 'application/json'
        });
        res.end(body);
    });
})


var server = app.listen(app.get("port"), function() {

    var host = server.address().address;
    var port = server.address().port;

    console.log("Node.js API app listening at http://%s:%s", host, port);

});