const request = require('request');

const vaccinationPlaces = {
    "Hälsan 2": 1187,
    "Råslätt": 1201,
    "Spira": 1259,
    "Equmeniakyrkan": 1198,

}
const vaccinationPlace = vaccinationPlaces[process.argv[2]];
console.log("vaccinationPlace: ", process.argv[2], " - ", vaccinationPlace);

const vaccinationUrl = "https://booking-api.mittvaccin.se/clinique/" + vaccinationPlace + "/appointments/8225/slots/210616-210627"
console.log("vaccinationUrl: ", vaccinationUrl);
// const vaccineURL = "https://booking-api.mittvaccin.se/clinique/1187/appointments/8225/slots/210616-210627"
// const vaccineURLSample = "https://booking-api.mittvaccin.se/clinique/1187/appointments/8225/slots/210616-210704"

function requestTimes() {
    request(vaccinationUrl, function(error, response, body) {
        console.log('\033[2J');
        console.log("PLACE: ", process.argv[2]);
        const jsonResponse = JSON.parse(body);
        jsonResponse.forEach(dateObject => {
            const availableTimes = dateObject.slots.filter(obj => obj.available);
            if(availableTimes.length !== 0) {
                console.log(dateObject.date + " HasTime: ", availableTimes , "https://bokning.mittvaccin.se/klinik/"+vaccinationPlace+"/bokning2")
            } else {
                console.log(dateObject.date + " No available times");
            }
        });
    });

}

setInterval(() => {
    requestTimes();
}, 5000);