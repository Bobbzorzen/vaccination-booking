// Importing JavaScript
//
// You have two choices for including Bootstrap's JS files—the whole thing,
// or just the bits that you need.


// Option 1
//
// Import Bootstrap's bundle (all of Bootstrap's JS + Popper.js dependency)

// import "../../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js";


// Option 2
//
// Import just what we need

// If you're importing tooltips or popovers, be sure to include our Popper.js dependency
// import "../../node_modules/popper.js/dist/popper.min.js";

import "../../node_modules/bootstrap/js/dist/util.js";
import "../../node_modules/bootstrap/js/dist/modal.js";

const municipality = "Jönköpings_kommun";
let currentPollId;

const vaccinationPlaces = {
    "Hälsan 2": 1187,
    "Råslätt": 1201,
    "Spira": 1259,
    "Equmeniakyrkan": 1198,

}
const vaccinationPlace = vaccinationPlaces["Hälsan 2"];
const startDate = "210616";
const endDate = "210627";


const vaccinationUrl = "https://booking-api.mittvaccin.se/clinique/" + vaccinationPlace + "/appointments/8225/slots/" + startDate + "-" + endDate;
const bookingUrl = "https://bokning.mittvaccin.se/klinik/<careCenterId>/bokning2";
const allCareCentersUrl = "http://127.0.0.1:5501/care-centers";

function populateCareCentersList(careCentersData) {
    const municipalityName = careCentersData.Name;
    const careCenters = careCentersData.Clinics.filter((clinic) => clinic.Id);
    const careCenterList = $(".care-centers-list");
    $(".municipality-header").text(municipalityName);
    careCenters.forEach(careCenter => {
        const inputWrapper = $(" <div class='form-check'></div>");
        const input = $("<input class='form-check-input' type='checkbox' value='" + careCenter.Id + "' id='" + careCenter.Id + "'></input>");
        const label = $("<label class='form-check-label' for='" + careCenter.Id + "'>" + careCenter.DisplayName + "</label>");
        inputWrapper.append(input);
        inputWrapper.append(label);
        careCenterList.append(inputWrapper);
    });
}

function requestAllCareCenters() {
    $.ajax({
        dataType: "json",
        url: allCareCentersUrl,
        success: (data) => {
            const filteredMunicipalityData = data.find((object) => object.IdName === municipality)
            console.log("data?? ", filteredMunicipalityData);
            populateCareCentersList(filteredMunicipalityData);

        }
    });
}

function printAvailableTimesToScreen(availableTimes) {
    const availableTimesWrapper = $(".available-times");
    availableTimesWrapper.empty();
    availableTimes.forEach((day) => {
        const dayHeader = $("<h4>Date: " + day.date + "</h4>")
        const slotsWrapper = $("<div class='slots-wrapper'></div>");
        day.slots.forEach(slot => {
            const url = bookingUrl.replace("<careCenterId>", day.id)
            const slotElement = $("<a href='" + url + "' target='_blank' class='slot' data-carecenter='" + day.id + "'>" + slot.when + "</a>")
            slotsWrapper.append(slotElement);
        });
        availableTimesWrapper.append(dayHeader);
        availableTimesWrapper.append(slotsWrapper);
        console.log("day: ", day);
    });
}

function requestAvailableTimesFromCareCenters() {
    const selectedCareCenters = $.map($("input[type='checkbox']:checked"), (n, i) => n.id);
    const requestsToDeferr = selectedCareCenters.map((careCenterId) => $.ajax(allCareCentersUrl + "/" + careCenterId));
    console.log("checked checkboxes? ", selectedCareCenters);
    $.when(...requestsToDeferr).then((...args) => {
        args.forEach((arg, index) => {
            arg[0] = arg[0].map((item) => {
                item.id = selectedCareCenters[index]
                return item;
            });
        });
        const allResults = args.map((resultArray) => resultArray[0]).flat();
        allResults.forEach((item) => {
            item.slots = item.slots.filter((slot) => slot.available)
        });
        const filteredResults = allResults.filter((result) => result.slots.length > 0);
        printAvailableTimesToScreen(filteredResults);
    });
}

function pollAllSelectedCareCenters() {
    // currentPollId = setTimeout(requestAvailableTimesFromCareCenters, 5000);
    currentPollId = setInterval(requestAvailableTimesFromCareCenters, 5000);
}

requestAllCareCenters();
pollAllSelectedCareCenters();